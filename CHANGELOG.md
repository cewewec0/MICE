# MICE Changelog

## v0.6.5
* Repo changes
    * CI/CD pipeline
    * NPM
* Hotfix for breaking script when textContent not detected; I can't get it to happen but hopefully this will fix it. Could always just remove this section.

## v0.6.4
* Melvor -> v0.20

## v0.6.3
* Melvor -> v0.19.2
* Update build script terminology
* Update insta-kill to prevent console errors
* Update mastery cheat activation notification to be multi-line for readability

## v0.6.2
### Extremely minor change
* Melvor -> v0.19.1 now. Updated MICE to reflect! Everything seems to work still (and Agility skill-up was even automatically implemented. Beautiful)

## v0.6.1
### Very minor change
* Mastery tooltip on the cheat button now warns of changing multiplier; the cheat is no longer active when you either close the mastery page or try to change the multiplier (+1, +5, +10)

### New Mastery Cheat
* Also, for related flubs and just general cheatness, there's now a Max Pool XP cheat as well.

## v0.6
### Melvor @ v0.18.2!
* Long time no update, but WOW, the amount of users on the Chrome store is CRAZY! Here's an update for you folk.
* Updated `manifest.json` match pattern to avoid loading the cheat engine on the Melvor Wiki!

### Brand New!
* Long-awaited Mastery Cheat!
    * Hijacks the Mastery Pool spending page to cheat mastery without needing pool XP. Neat!

## v0.5.6
### Melvor updated to v0.18!
* Everything still seems to work. Version check changed, no longer alerts on load. Party on.

## v0.5.5
### Melvor updated to v0.17!
* Fixed custom notification function to use Toastify.
* Fixed add loot cheats.
* Added more notifications upon incorrect cheat usage.
* Update build script to automatically name with version.
* Alerts when trying to load MICE and it detects it's already on page.

## v0.5.4
### Cosmetic fixes
* Fix a weirdness in the cheat menu skill selection buttons: m-1 class was being removed on selection, changed the way selection function worked to only work on classes it needs to.
* Fixed the farming page 'bonemeal' buttons to look consistent with the farming area buttons.

## v0.5.3
* Kill android: remove browser-specific-settings in manifest

## v0.5.2
* Minor bugfix: 'Magic Dust' was not canceling after turning off, leaving the potential for many overlapping instances of autoattacking, kinda breaking combat. Moved the scope of the variable containing the interval, all seems good again.

## v0.5.1
* More cleanup
* Version detection through manifest
* Notification of menu enabled at bottom of page

## v0.5
### Core changes
* Wowee a whole core thing for a stinky cheet
* Moving code out of main content script into injection scripts
* Refactoring & such, cleaning comments, general code improvements
* Cheat menu no longer its own page, just adds on to whatever page you're on again
* New combat button & slightly cleaner buttons

## v0.4.4
### Core changes
* Moved cheat menu to the top near the red cheat header, above bank & shop
* All buttons are hidden automatically, need to click the eye to show cheat tools now
### Repository changes
* Moved shit to Changelog
* Cleanup in general
* Build script

### v0.4.3
Minor fixes to major bugs, now compatible with Melvor v0.16.2+

### v0.4.2
Long time, eh?
* Fixes
    * Extra Attack button works
    * Magic Dust works

## v0.4.1
* UI Fixes:
    * Show List of Loot button repaired. Function toString no longer supported in firefox? Moving to JSON.stringify() since it's included in Melvor.
    * List of Loot UI update: just made the div 100% wide, why not.
    * Cheaty Combat buttons moved
    * Cheat Menu Nav Button now has status text, won't close unless you're on another page and you click the nav button to 'off'
* Code Changes:
    * Moved most of UI injections into a setup function
* Melvor Updated! (alpha v0.14)
    * No more Unlimited Change Username: the limit has been removed, the cheaty interface is gone.

v0.3.14
* Small cosmetic changes
* Big-ish oopsie corrected: was passing strings into the id of items got through the loot ID cheat, probably causing some strange errors. Fixed, and now IDs of cheated items are correctly recognized.
    * This is why you back up your saves before using such crazy tools!

### v0.3.13
* Melvor Idle has updated!
* Removed banner ad manipulation (settings menu has changed)
* Various fixes and tweaks

### Changes in De-auto/v0.3.100
* Removed all automation scripts and left just the cheats
* Added hide cheat sidebar/nav menu functionality
* Cleaned up cheat sidebar/nav UI
