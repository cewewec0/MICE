# Melvor Idle Cheat Engine

### **MICE** is an **UNOFFICIAL** Firefox & Chrome browser extension for the fun idle web game Melvor Idle.

This is best used as a save recovery tool or game testing helper.

**BACK UP YOUR SAVES BEFORE MODIFYING YOUR GAME!**

[![Mozilla Firefox](https://img.shields.io/amo/v/melvor-idle-cheat-engine?label=Get%20MICE%3A%20Firefox%20Add-on&logo=Firefox)](https://addons.mozilla.org/en-US/firefox/addon/melvor-idle-cheat-engine/)
[![Google Chrome](https://img.shields.io/chrome-web-store/v/cbbaeikkmjnompmdodelbibeaibdelmm?label=Get%20MICE%3A%20Chrome%20Extension&logo=Google%20Chrome)](https://chrome.google.com/webstore/detail/melvor-idle-cheat-engine/cbbaeikkmjnompmdodelbibeaibdelmm?hl=en&authuser=1)

## MICE helps you manipulate the game and can do these things:

* New: Add Mastery Levels!! Check the Mastery Pool page for a hijack.
* New: Max Mastery Pool XP cheat as well!
* Add gold
* Add Slayer Coins
* Add loot
* List of all Loot IDs and names to use the loot cheats
* Level up a skill
* Instantly grow your crops, herbs, or trees with new buttons in the farming page
* Instantly attack your enemies without limits with a savage, dirty button in the combat page
* Use magic dust to decimate your enemies with unbelievable *speed*
* Insta-Kill button

### What happened to that sweet, sweet automation?!

It moved to this extension, which is available [here as Scripting Engine for Melvor Idle!](https://gitlab.com/aldousWatts/SEMI)

MICE and SEMI are compatible, meaning they can both be run simultaneously. So if you're looking to cheat AND automate/improve QOL, it's easy with both.

## Alternative install

Download the entire MICE directory, including the .js files as well as icon folder.

### Firefox

Open the url "about:debugging" without quotes, enable add-on debugging if you need to.

On newest firefox, go to the "this firefox" page.

Install the addon by clicking "Load Temporary Add-on" on the top right and opening manifest.json.

### Chrome

Open settings > tools > extensions, enable developer options in the top right.

Click "Load Unpacked" in the top left.

Open the MICE containing folder that has manifest.json to load the extension.
