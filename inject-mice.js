//Core
var MICE = (() => {
  const VERSION = MICE_VERSION;
  const GAME_VERSION = "Alpha v0.20";
  const config = {
    cheatborder: true,
    menuOn: false,
    navMenu: true,
    cheatInfoVisible: true,
    magicDustOn: false,
    skillPick: 22,
  };
  const iconSrc = $("#MICE-icon").attr("src");

  //MICE functions
  /**
   * @param {*} x
   * @param {*} y
   */
  const mergeOnto = (x, y) => {
    Object.keys(y).forEach((key) => {
      x[key] = y[key];
    });
  };
  /**
   * @param {string} x
   * @param {*} y
   */
  const setItem = (x, y) => {
    localStorage.setItem(`MICE-${x}`, JSON.stringify(y));
  };
  /** @param {string} x */
  const getItem = (x) => {
    const y = JSON.parse(localStorage.getItem(`MICE-${x}`));
    return y;
  };
  /**
   * Custom notifications! Outputs a custom notification with optional first image, MICE icon, and message.
   * @param {string} imgsrc
   * @param {string} msg
   * @param {number} n
   */
  const notify = (
    imgsrc = "assets/media/skills/magic/magic.svg",
    msg = "Custom Notifications!",
    n = 3000
  ) => {
    Toastify({
      text: `<div class="text-center"><img class="notification-img" src="${imgsrc}"><img src="${iconSrc}" height="auto" width="auto" style="margin: 4px;"><span class="badge badge-success"> ${msg} </span></div>`,
      duration: n,
      gravity: "bottom", // `top` or `bottom`
      position: "center", // `left`, `center` or `right`
      backgroundColor: "transparent",
      stopOnFocus: false,
    }).showToast();
  };
  const showItems = () => {
    const itemSplit = JSON.stringify(CONSTANTS.item).split(",").join("\n");
    const list = "List of items and IDs: \n" + itemSplit;
    const listBox = document.createElement("pre"); //use pre html tag for the string's new lines to format properly
    listBox.style =
      "height:333px; width: 100%; border: 1px solid #ccc; font:16px/26px monospace, monospace; overflow: auto; color:cyan;";
    listBox.textContent = list;
    $("#cheat-menu").append(listBox);
    $("#show-items-btn").remove();
    $("#list-header").text("Ctrl+F is your friend here. :)");
  };
  const toggleborder = () => {
    MICE.config.cheatborder = !MICE.config.cheatborder;
    $(".content-side.content-side-full").css(
      "border",
      MICE.config.cheatborder ? "2px solid red" : ""
    );
  };
  const openCheatMenu = () => {
    MICE.config.menuOn = !MICE.config.menuOn;
    $("#cheat-container").attr(
      "class",
      `content ${MICE.config.menuOn ? "" : "d-none"}`
    );
    $("#cheat-menu-status").text(MICE.config.menuOn ? "On" : "Off");
    $('[data-toggle="tooltip"]').tooltip("hide");
    if (MICE.config.menuOn)
      MICE.notify(
        "assets/media/main/settings_header.svg",
        "Cheat Menu enabled at the bottom of the main page.",
        7000
      );
  };
  const giveGPcheatNav = () => {
    $("#modal-gp-cheat").modal(open);
  };
  const giveSCcheatNav = () => {
    $("#modal-sc-cheat").modal(open);
  };
  const toggleNavMenu = () => {
    MICE.config.navMenu = !MICE.config.navMenu;
    $(".nav-main-heading:contains('Cheat Tools')")
      .nextAll()
      .slice(0, 6)
      .toggleClass("d-none");
    $("#mice-hider").attr(
      "class",
      `far fa-eye${MICE.config.navMenu ? "" : "-slash"} text-muted ml-1`
    );
  };
  const toggleInfo = () => {
    MICE.config.cheatInfoVisible = !MICE.config.cheatInfoVisible;
    $("#hide-cheat-info-btn").text(
      MICE.config.cheatInfoVisible ? "Hide Info" : "Show Info"
    );
    MICE.config.cheatInfoVisible
      ? $("#cheat-menu-info").show()
      : $("#cheat-menu-info").hide();
  };
  const updateGetGP = () => {
    //immediately updates custom nav button gp amount
    $("#nav-current-gp2").text(convertGP(gp) + " GP");
    $("#nav-current-gp2").attr(
      "data-original-title",
      numberWithCommas(gp) + " GP"
    );
    $("#nav-current-sc").text(convertGP(slayerCoins) + " SC"); //and I'm just gonna add SC in here too
    $("#nav-current-sc").attr(
      "data-original-title",
      numberWithCommas(slayerCoins) + " SC"
    );
  };
  const cheatgpupdateloop = setInterval(() => {
    updateGetGP();
  }, 3000); //looping function to constantly update the cheat clone gp count & sc count

  //Cheat functions
  const instaKillEnemy = () => {
    if (enemyInCombat == null) return;
    else {
      damageEnemy(69000);
    }
  };
  /**
   * Cheat to instantly grow all Allotments, Herbs, or Trees.
   * @param {number} x - 0 for allotments, 1 herbs, 2 trees.
   */
  const instantFarm = (x = 0) => {
    //
    MICE.notify(
      "assets/media/skills/farming/farming.svg",
      "Bonemeal has been added. Please wait for success."
    );
    for (i = 0; i < newFarmingAreas[x].patches.length; i++) {
      newFarmingAreas[x].patches[i].timePlanted = 1; //sets all patches to time immemorial (1970) where all plants are ready to harvest now
    }
  };
  const lootcheat = () => {
    var lootnameask = prompt(
      "What loot do you want? Type or copy the name here. Use underscores for spaces, delete any parentheses.",
      "Normal_Logs"
    );
    if (lootnameask === null || CONSTANTS.item[lootnameask] === undefined)
      return notify(
        undefined,
        `You entered an invalid loot name or cancelled the cheat.`
      );
    var lootqtyask = prompt("How many?", "1");
    var lootqty = Number(lootqtyask);
    if (isNaN(lootqty) || lootqty <= 0)
      return notify(
        undefined,
        "You either entered a non-number or a number <= 0. No changes were made."
      );
    addItemToBank(CONSTANTS.item[lootnameask], lootqty);
  };
  const lootcheatID = () => {
    var lootidask = prompt("Enter the Loot ID of the item you want.", "222");
    var lootid = Number(lootidask);
    if (
      isNaN(lootid) ||
      lootid < 0 ||
      lootidask === null ||
      items[lootid] === undefined
    )
      return notify(
        undefined,
        `You either entered a non-number, a number < 0, an invalid loot ID, or you cancelled the cheat. No changes were made.`
      );
    var lootqtyask = prompt("How many?", "222");
    var lootqty = Number(lootqtyask);
    if (isNaN(lootqty) || lootqty <= 0)
      return notify(
        undefined,
        "You either entered a non-number or a number <= 0. No changes were made."
      );
    addItemToBank(lootid, lootqty);
  };
  var magicDust;
  const giveDust = () => {
    MICE.config.magicDustOn = !MICE.config.magicDustOn;
    $("#magic-dust-status").text(
      MICE.config.magicDustOn ? "Feelin' it" : "No Dust"
    );
    if (MICE.config.magicDustOn) {
      magicDust = setInterval(() => {
        if (enemyInCombat && combatData.enemy.hitpoints > 0) attackEnemy();
      }, 1000);
      MICE.notify(
        "assets/media/bank/holy_dust.svg",
        "WOW! What a rush! You feel a surging, stimulating power.",
        5000
      );
    } else {
      clearInterval(magicDust);
      MICE.notify(
        "assets/media/bank/holy_dust.svg",
        "You put away the good stuff and drink some water. Here comes sobriety."
      );
    }
  };
  /** @param {number} x skill ID */
  const cheatSkill = (x) => {
    //de-highlight previous selection
    $("#skill-btn-" + MICE.config.skillPick).removeClass("btn-primary");
    $("#skill-btn-" + MICE.config.skillPick).addClass("btn-outline-primary");
    MICE.config.skillPick = x; //update selection
    //highlight selection
    $("#skill-btn-" + x).removeClass("btn-outline-primary");
    $("#skill-btn-" + x).addClass("btn-primary");
  };
  const levelUpCheat = () => {
    if (MICE.config.skillPick === 22) return;
    addXP(
      MICE.config.skillPick,
      exp.level_to_xp(skillLevel[MICE.config.skillPick] + 1) +
        1 -
        skillXP[MICE.config.skillPick]
    );
    updateLevelProgress(MICE.config.skillPick);
    updateSkillWindow(MICE.config.skillPick);
  };
  const giveGPcheat = () => {
    const gpaddnum = Number(document.getElementById("GPask").value);
    gpAdd(gpaddnum);
  };
  /** @param {number} n - amount of gold to add to player's bank */
  const gpAdd = (n) => {
    if (!isNaN(n) && n > 0) {
      gp += n;
      updateGP();
      gpNotify(n); //unearned gp stats? lol
      updateGetGP();
    } else {
      notify(undefined, `No changes made! Enter a valid number!`);
    }
  };
  /** @param {number} n - amount of slayer coins to add to player's bank */
  const giveSCcheat = (n) => {
    n = Number(n); //just in case
    if (!isNaN(n) && n > 0) {
      slayerCoins += n;
      notifySlayer(`+${numberWithCommas(n)} SC added!`);
      updateSlayerCoins();
      updateGetGP();
    } else {
      notify(undefined, `No changes made! Enter a valid number!`);
    }
  };

  /** potential mastery cheat
   * @param {number} skill
   * @param {number} masteryID
   */
  const masteryCheat = (skill, masteryID) => {
    let xp = getMasteryXpForNextLevel(skill, masteryID);
    addMasteryXP(skill, masteryID, 0, true, xp, false);
    updateSpendMasteryScreen(skill, masteryID);
  };

  const activateMasteryCheat = () => {
    if ($("#mastery-cheating").length > 0) {
      return;
    }
    //set mastery upgrades to +1
    $("#modal-spend-mastery-xp .btn.m-1:first").click();
    //select all buttons currently displayed in spend xp modal
    const currentMasteryBtns = $(
      "#modal-spend-mastery-xp .btn:not('.m-1'):not('.m-2')"
    );

    //change all their onclicks to MICE.masteryCheat based on their onclick params
    for (i = 0; i < currentMasteryBtns.length; i++) {
      const oncl = currentMasteryBtns[i].getAttribute("onclick");
      const arr = oncl.split("(")[1].split(",");
      currentMasteryBtns[i].onclick = () => {
        MICE.masteryCheat(Number(arr[0]), Number(arr[1]));
      };
      currentMasteryBtns[i].style = "border: 3px solid aqua;";
      //undisable them
      if (currentMasteryBtns[i].disabled) {
        currentMasteryBtns[i].disabled = false;
        currentMasteryBtns[i].classList.remove("disabled");
      }
    }
    //when you change the mastery page or close and reload it, wham, it's back to normal
    $("#modal-content-spend-mastery-xp").append(
      $(`<div id="mastery-cheating"></div>`)
    );
    notify(
      "https://cdn.melvor.net/core/v018/assets/media/main/mastery_header.svg",
      "Mastery Cheat Activated!<br>No longer needing or spending Pool XP to increase mastery<br>UNTIL you close the page<br>OR click a multiplier in the top right (+1, +5, or +10).",
      5000
    );
  };

  const maxMasteryPool = () => {
    const currentMasteryBtns = $(
      "#modal-spend-mastery-xp .btn:not('.m-1'):not('.m-2')"
    );
    const oncl = currentMasteryBtns[0].getAttribute("onclick");
    const arr = oncl.split("(")[1].split(",");
    const currentMasterySkill = Number(arr[0]);
    addMasteryXPToPool(currentMasterySkill, 10000000000, false, true);
    notify(
      "https://cdn.melvor.net/core/v018/assets/media/main/mastery_header.svg",
      "You have max Pool XP!",
      2000
    );
  };

  //UI Setup
  const MICEsetup = () => {
    //If UI already present, don't inject
    if ($("#mice-hider").length) return;

    //Check and confirm continue with mismatch game function
    if (gameVersion !== GAME_VERSION) {
      const response =
        confirm(`MICE v${VERSION} was built for Melvor Idle ${GAME_VERSION}, but the game is currently on ${gameVersion}.
      MICE may experience errors in newer versions of Melvor.
      Do you want to try loading MICE anyway?`);
      if (!response) {
        console.log("MICE was prevented from loading.");
        notifyPlayer(10, "MICE was prevented from loading.");
        return;
      }
    }

    $(".content-side.content-side-full").css("border", "2px solid red"); //nav border
    const testCheatHeader =
      document.getElementsByClassName("nav-main-heading")[0];
    if (testCheatHeader.length !== 0) {
      testCheatHeader.textContent = `Cheats On: MICE v${VERSION}`;
      testCheatHeader.className =
        "nav-main-heading text-uppercase text-danger mice-head"; //makes visible, deletes d-none from class
    }

    //Sidebar injection
    $("#test-env").parent()
      .append(`<li class="nav-main-heading" style="font-size: 12pt; color: cyan;" title="Ooooh, we cheatin' now!" id="mice-heading">
      Cheat Tools
      <a onclick="MICE.toggleNavMenu()">
        <i style="color: cyan !important;" class="far fa-eye text-muted ml-1" id="mice-hider"></i>
      </a>
    </li>
    <li class="nav-main-item">
      <a class="nav-main-link nav-compact" onclick="MICE.openCheatMenu()">
      <img class="nav-img" src="${iconSrc}">
      <span class="nav-main-link-name" style="color: cyan;">Cheat Menu</span>
      <small id="cheat-menu-status">Off</small></a>
    </li>
    <li class="nav-main-item">
      <a class="nav-main-link nav-compact" onclick="MICE.giveGPcheatNav()">
        <img class="nav-img" src="assets/media/main/gp.svg">
        <span class="nav-main-link-name">Get GP</span><img src="assets/media/main/coins.svg" style="margin-right: 4px;" width="16px" height="16px">
        <small id="nav-current-gp2" class="js-tooltip-enabled" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="971,226,398 GP">971M GP</small>
      </a>
    </li>
    <li class="nav-main-item">
      <a class="nav-main-link nav-compact" onclick="MICE.giveSCcheatNav()">
        <img class="nav-img" src="assets/media/main/slayer_coins.svg">
        <span class="nav-main-link-name">Get SC</span>
        <img src="assets/media/main/slayer_coins.svg" style="margin-right: 4px;" width="16px" height="16px">
        <small id="nav-current-sc" class="js-tooltip-enabled" data-toggle="tooltip" data-html="true" data-placement="bottom" title="" data-original-title="1,299,919,563 SC">1,299M SC</small>
      </a>
    </li>
    <li class="nav-main-item">
      <a class="nav-main-link nav-compact" onclick="MICE.lootcheat()">
        <img class="nav-img" src="assets/media/bank/elite_chest.svg">
        <span class="nav-main-link-name">Get Loot by Name</span>
      </a>
    </li>
    <li class="nav-main-item">
      <a class="nav-main-link nav-compact" onclick="MICE.lootcheatID()">
        <img class="nav-img" src="assets/media/bank/elite_chest.svg">
        <span class="nav-main-link-name">Get Loot by ID</span>
      </a>
    </li>
    <li class="nav-main-item">
      <a class="nav-main-link nav-compact" onclick="MICE.toggleborder()">
        <img class="nav-img" src="assets/media/skills/combat/ship.svg">
        <span class="nav-main-link-name">Toggle Red Border</span>
      </a>
    </li>`);

    //Cheat menu injection
    $("main").append(`<div class="content d-none" id="cheat-container">
      <div class="row row-deck">
        <div class="col-md-12">
          <div class="block block-rounded block-link-pop border-top border-settings border-4x">
            <div class="block-content" id="cheat-menu" style="border: 2px solid cyan; border-radius: 4px; background-color: rgb(36, 36, 36);">
              <h2 class="content-heading border-bottom mb-4 pb-2" style="color: cyan; font-size: 14pt;">MICE v${VERSION} Cheat Menu!</h2>
              This menu persists across any page, click the Cheat Menu button again to turn it off.
              <br><br>
              <button id="hide-cheat-info-btn" class="btn btn-outline-primary" type="button" onclick="MICE.toggleInfo()">Hide Info</button>
              <div id="cheat-menu-info" style="height: 222px; padding: 10px; width: 100%; border: 1px solid rgb(204, 204, 204); white-space: pre-wrap; overflow: auto; color: cyan;"></div>
              <h2 class="content-heading border-bottom mb-4 pb-2" style="color: cyan; font-size: 10pt;" id="skill-cheat-header">Skill Level-up Cheat</h2>
              <div class="row push" id="skill-cheat-div">
                <div class="col-6 col-lg-4">
                  <p class="font-size-sm text-muted">Level Up Cheat! Skill to modify:</p>
                </div>
                <div class="col-6 col-lg-8" id="skill-selector">
                </div>
              </div>
              <div class="row push">
                <div class="col-6 col-lg-4">
                  <p class="font-size-sm text-muted">Level up once:</p>
                </div>
                <div class="col-6 col-lg-8">
                  <button id="level-up-btn" class="btn btn-outline-primary" type="button" onclick="MICE.levelUpCheat()">Level Up Selected Skill Once</button>
                </div>
              </div>
              <h2 class="content-heading border-bottom mb-4 pb-2" style="color: cyan; font-size: 10pt;" id="list-header">The Button Below Shows THE List of All Loot Items</h2>
              <button id="show-items-btn" class="btn btn-outline-primary" type="button" style="margin-bottom: 20px;" onclick="MICE.showItems()">
                <img src="assets/media/main/bank_header.svg" width="32" height="32">
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>`);

    /**
     * Helper function - creates skill selector buttons for the level up cheat.
     * @param {number} n - where n is skill ID.
     */
    const skillSelectBtn = (n) => {
      return $(`<button id="skill-btn-${n}" class="btn btn-outline-primary m-1" type="button" onclick="MICE.cheatSkill(${n});">
        <img src="${SKILLS[n].media}" width="32" height="32">
      </button>`);
    };
    for (skill in SKILLS) $("#skill-selector").append(skillSelectBtn(skill));

    //Cheat menu info injection
    $("#cheat-menu-info").append(
      $(`<p>Melvor Idle Cheat Engine v${VERSION} is running. Sweet!
This version of MICE was built on Melvor Idle ${GAME_VERSION}.

The red sidebar border is a friendly reminder that MICE is running, but can be turned off.

Want automation & QOL additions too? Check out...
<a href="https://gitlab.com/aldousWatts/SEMI" target="_blank">Scripting Engine for Melvor Idle!</a>

-BEWARE, YE CHEATER!-
Use such software at your own risk! Back up ye game before modifying!
This extension may completely break the save if used wrongly. You have been warned.
Also, be careful about using Ctrl+F5 with this game, I've had it completely corrupt a save. That may or may not be the impetus for cheating. ;)

-Tips-
* Don't forget there are extra buttons added by MICE in the Farming & Combat pages.
* GP/SC cheats won't run if you've entered text or negative numbers to help prevent breaking the game data. Strings and ints mix badly.
* You should leave page alerts on because MICE uses js prompts for certain cheats at this point. (Moving to modals like GP/SC now)

-Thanks and More-
* Huge thanks to Mr Frux, the developer who created this awesome game!
* Javascript and JQuery and all the developers everywhere
* Thank YOU for using this software, and I hope you enjoy it!

Have fun! :D</p>`)
    );

    //Modal Dialog for GP cheat!
    $("#modal-account-change").before(
      $(`<div class="modal" id="modal-gp-cheat" tabindex="-1" role="dialog" aria-labelledby="modal-block-normal" aria-hidden="true" style="display: none;">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="block block-themed block-transparent mb-0">
            <div class="block-header bg-primary-dark">
              <img class="nav-img" src="assets/media/main/coins.svg">
              <h3 class="block-title">Pirate some GP</h3>
              <div class="block-options">
                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                  <i class="fa fa-fw fa-times"></i>
                </button>
              </div>
            </div>
            <div class="block-content font-size-sm">
              <p id="name-change-text"></p>
              <div id="GPaskForm" class="form-group">
                <label for="example-text-input">How many pirated Gold Pieces would you like?</label>
                <input class="form-control" id="GPask" placeholder="1234567890" type="number">
              </div>
            </div>
            <div class="block-content block-content-full text-right border-top">
              <button type="button" id="gp-cheat-nav-btn" class="btn btn-sm btn-primary" data-dismiss="modal" onclick="MICE.giveGPcheat();"><i class="fa fa-check mr-1"></i>Plunder Booty!</button>
            </div>
          </div>
        </div>
      </div>
    </div>`)
    );

    //SC Cheat Modal Dialog -- adds modified modal into the page before the account change one, which it's based on.
    $("#modal-account-change").before(
      $(`<div class="modal" id="modal-sc-cheat" tabindex="-1" role="dialog" aria-labelledby="modal-block-normal" aria-hidden="true" style="display: none;">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="block block-themed block-transparent mb-0">
            <div class="block-header bg-primary-dark">
              <img class="nav-img" src="assets/media/main/slayer_coins.svg">
              <h3 class="block-title">Cheatin' fer Slayer Coins</h3>
              <div class="block-options">
                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                  <i class="fa fa-fw fa-times"></i>
                </button>
              </div>
            </div>
            <div class="block-content font-size-sm">
              <p id="name-change-text"></p>
              <div id="SCaskForm" class="form-group">
                <label for="example-text-input">How many ill-gotten Slayer Coins would you like?</label>
                <input class="form-control" id="SCask" placeholder="1234567890" type="number">
              </div>
            </div>
            <div class="block-content block-content-full text-right border-top">
              <button type="button" id="sc-cheat-nav-btn" class="btn btn-sm btn-primary" data-dismiss="modal" onclick="MICE.giveSCcheat(Number(document.getElementById('SCask').value));"><i class="fa fa-check mr-1"></i>Gimme!</button>
            </div>
          </div>
        </div>
      </div>
    </div>`)
    );

    //Mastery cheat button
    const activateMasteryCheat =
      $(`<div id="activate-mastery-cheat" class="block block-content block-rounded block-link-pop border-top border-farming border-4x text-center" style="padding: 0;">
      <button id="activate-mastery-cheat-btn" class="btn m-2" style="background: rgb(92, 172, 229);" title="When you push this button, you no longer need Mastery Pool XP to increase your mastery for an item. You will no longer spend any Mastery Pool XP you have to increase the level. If you close this page OR select a multiplier for mastery (+1, +5, +10), this effect will be nullified, and you'll have to click Activate again before cheating.">[MICE] Activate Mastery Cheat</button>
      <button id="max-mastery-pool-btn" class="btn btn-warning m-2" title="Maximize your Mastery Pool XP for this skill!">[MICE] Give Max Mastery Pool XP</button>
    </div>`);
    activateMasteryCheat.find("#activate-mastery-cheat-btn").on("click", () => {
      MICE.activateMasteryCheat();
    });
    activateMasteryCheat.find("#max-mastery-pool-btn").on("click", () => {
      MICE.maxMasteryPool();
    });

    $("#modal-spend-mastery-xp .block-header").after(activateMasteryCheat);

    //Combat cheat buttons
    const combatButtonAnchor = $("#combat-attack-style-0").parent().parent();
    combatButtonAnchor.append(`<div class="col-12">
      <button type="button" class="btn btn-outline-secondary m-1" id="extra-atk-btn" style="width: 100%; background-color: orange;" onclick="if (enemyInCombat && combatData.enemy.hitpoints>0) attackEnemy()">
      <img class="nav-img" id="combat-attack-style-img-0" src="assets/media/skills/slayer/slayer.svg">
      <span id="combat-attack-style-name-0">Instant Attack</span>
      </button>
    </div>`);
    combatButtonAnchor.append(`<div class="col-12">
      <button type="button" class="btn btn-outline-secondary m-1 btn-danger" id="insta-kill-btn" style="width: 100%;" onclick="MICE.instaKillEnemy()">
      <img class="nav-img" id="combat-attack-style-img-0" src="assets/media/bank/magic_bones.svg">
      <span id="combat-attack-style-name-0">InstaKill</span>
      </button>
    </div>`);
    combatButtonAnchor.append(`<div class="col-12">
      <button type="button" class="btn btn-success m-1" onclick="MICE.giveDust()" title="Does an extra attack every half second." style="width: 100%;">Toggle Magic Dust! [<span id="magic-dust-status">?</span>]</button>
    </div>`);

    //Farming cheat bonemeal button injection
    $("#farming-container").append(`<div class="row row-deck gutters-tiny">
      <div class="col-12 col-md-6 col-xl-4">
        <a class="block block-content block-rounded block-link-pop border-top border-farming border-4x" onclick="MICE.instantFarm(0)">
          <div class="media d-flex align-items-center push">
            <div class="mr-3">
              <img class="shop-img" src="assets/media/bank/bones.svg">
            </div>
            <div class="media-body">
              <div class="font-w600">Grow Allotments Now</div>
              <div class="font-size-sm">Apply MICE bonemeal (instantly grows plants) to your Allotments.</div>
            </div>
          </div>
        </a>
      </div>
      <div class="col-12 col-md-6 col-xl-4">
        <a class="block block-content block-rounded block-link-pop border-top border-farming border-4x" onclick="MICE.instantFarm(1)">
          <div class="media d-flex align-items-center push">
            <div class="mr-3">
              <img class="shop-img" src="assets/media/bank/bones.svg">
            </div>
            <div class="media-body">
              <div class="font-w600">Grow Herbs Now</div>
              <div class="font-size-sm">Apply MICE bonemeal (instantly grows plants) to your Herbs.</div>
            </div>
          </div>
        </a>
      </div>
      <div class="col-12 col-md-6 col-xl-4">
        <a class="block block-content block-rounded block-link-pop border-top border-farming border-4x" onclick="MICE.instantFarm(2)">
          <div class="media d-flex align-items-center push">
            <div class="mr-3">
              <img class="shop-img" src="assets/media/bank/bones.svg">
            </div>
            <div class="media-body">
              <div class="font-w600">Grow Trees Now</div>
              <div class="font-size-sm">Apply MICE bonemeal (instantly grows plants) to your Trees.</div>
            </div>
          </div>
        </a>
      </div>
    </div>`);

    toggleNavMenu();

    //Loaded notification
    notify(
      "assets/media/monsters/dragon_black.svg",
      `Melvor Idle Cheat Engine v${VERSION} is now loaded and running!<br>
    Click the eye next to CHEAT TOOLS at the bottom of the sidebar for cheats.`,
      10000
    );
  };

  //setup after 5s
  setTimeout(() => {
    MICEsetup();
  }, 5000);

  return {
    VERSION,
    GAME_VERSION,
    config,
    iconSrc,
    mergeOnto,
    setItem,
    getItem,
    notify,
    showItems,
    toggleborder,
    openCheatMenu,
    giveGPcheatNav,
    giveSCcheatNav,
    toggleNavMenu,
    toggleInfo,
    instaKillEnemy,
    instantFarm,
    lootcheat,
    lootcheatID,
    giveDust,
    cheatSkill,
    levelUpCheat,
    giveGPcheat,
    giveSCcheat,
    masteryCheat,
    activateMasteryCheat,
    maxMasteryPool,
  };
})();
